using UnityEngine;
using UnityEngine.InputSystem;

public class CarRandomiserScript : MonoBehaviour
{
    public GameObject Car_01;
    public GameObject Car_02;
    public GameObject Car_03;
    public GameObject Car_04;
    public PlayerInputManager manager;

    private void Awake()
    {
        RandomisePrefabs();
    }
    public void OnPlayerJoined()
    {
        Debug.Log("Player Joining Detected");
        RandomisePrefabs();
    }

    void RandomisePrefabs()
    {
        int randomiser = Random.Range(1, 100);
        Debug.Log(randomiser);

        if (randomiser <= 25)
        {
            manager.playerPrefab = Car_01;
        }   
        else if (randomiser <= 50)
        {
            manager.playerPrefab = Car_02;
        }
        else if (randomiser <= 75)
        {
            manager.playerPrefab = Car_03;
        }
        else
        {
            manager.playerPrefab = Car_04;
        }
    }
}
