using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController_Testing_01 : MonoBehaviour
{
    [Header("Wheel Colliders")]
    public WheelCollider rearLeftColl;
    public WheelCollider rearRightColl;
    public WheelCollider frontLeftColl;
    public WheelCollider frontRightColl;

    [Header("Wheel Meshes")]
    public Transform rearLeftWheel;
    public Transform rearRightWheel;
    public Transform frontLeftWheel;
    public Transform frontRightWheel;

    public float steerStrength;
    public float horsePower;
    float speed;
    float steeringAngle;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        speed = Input.GetAxis("Vertical") * horsePower;
        steeringAngle = Input.GetAxis("Horizontal") * steerStrength;

        //The following code Updates Wheel meshes from wheel colliders
        UpdateWheels(rearRightColl, rearRightWheel);
        UpdateWheels(rearLeftColl, rearLeftWheel);
        UpdateWheels(frontRightColl, frontRightWheel);
        UpdateWheels(frontLeftColl, frontLeftWheel);
    }

    //Called 50 times a second. All physics systems should be inside this function
    private void FixedUpdate()
    {
        rearLeftColl.motorTorque = -speed;
        rearRightColl.motorTorque = -speed;
        frontLeftColl.motorTorque = -speed;
        frontRightColl.motorTorque = -speed;

        frontLeftColl.steerAngle = steeringAngle;
        frontRightColl.steerAngle = steeringAngle;
    }

    void UpdateWheels(WheelCollider source, Transform target)
    {
        Vector3 tempPosition;
        Quaternion tempRotaion;

        source.GetWorldPose(out tempPosition, out tempRotaion);

        target.position = tempPosition;
        target.rotation = tempRotaion;
    }
}
