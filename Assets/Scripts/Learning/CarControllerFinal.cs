using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;


public class CarControllerFinal : MonoBehaviour
{
    public Rigidbody CarBody;

    //Following are the driving variables to control car behaviour
    [Header("Driving Variables")]
    public float maxSteerAngle;
    public float brakeStrength;
    public float handBrakeStrength;
    
    [Header("Steering Response Tuning")] 
    public float steerResponseG1 = 0.5f;
    public float steerResponseG2 = 0.45f;
    public float steerResponseG3 = 0.4f;
    public float steerResponseG4 = 0.35f;
    public float steerResponseG5 = 0.3f;
    public float steerResponseR = 0.5f;


    //Following are the Engine Variables
    [Header("Engine Variables")]
    public float horsePower;
    public float idleRPM;
    public float redRPM;

    //Following are the Variables to adjust Torque;
    [Header("Applied Torque Adjustment")]
    public float apG1 = -1.2f;
    public float apG2 = -1.25f;
    public float apG3 = -1.3f;
    public float apG4 = -1.35f;
    public float apG5 = -1.4f;
    public float apGR = 1.4f;

    //Following are the variables related to nitrous
    [Header("Nitrous Variables")]
    public float nitroDuration; // How long the nitro will last in seconds
    public float nitroPower; // How much power will be added

    //Following variables hold the reference of the Wheel Colliders
    [Header("Wheel Colliders")]
    public WheelCollider wc_FrontLeft;
    public WheelCollider wc_FrontRight;
    public WheelCollider wc_RearLeft;
    public WheelCollider wc_RearRight;

    [Header("Wheel Meshes")]
    //Following are the variables that hold the references of the Wheel Mesh
    public Transform wm_FrontLeft;
    public Transform wm_FrontRight;
    public Transform wm_RearLeft;
    public Transform wm_RearRight;

    [Header("UI References")]
    public Image rpmBarUI;
    public Image nitrousBarUI;
    public TextMeshProUGUI speedUI;
    public TextMeshProUGUI gearUI;

    //Following are private vraibles necessary for deiving calculations and checks
    bool throttle;
    bool brake;
    bool handBrake;
    bool reverse;
    bool nitro;
    bool jump;

    //Variables for Nitrous
    float currentNitroPower; // variable being used a proxy in the Apply Torque formula. It's basically this variable that is set to a non-zero value to apply nitrous
    float currentNitroLeft; // The current amount of nitro left in the tank
    float nitroOffTime = 0; // The amount of time to wait before nitro starts replenishing
    float currentRPM; // The current RPM of the car
    float steeringInput; //The current steering input. Remains between -1 and 1 to describe which direction the tire is turning
    float currentVelo; //The speed of the car. Measure in the Update RPM function in Miles Per Hour
    
    //Variables to Reset Position
    Vector3 originalPos;
    Quaternion originalRot;

    //Following is the enum definition of Gears
    enum AvailableGears { eGear_1, eGear_2, eGear_3, eGear_4, eGear_5, eGear_reverse };

    //Following struct definition holds all data of the gears that can be engaged
    struct GearData
    {
        public AvailableGears currentGearName;
        public string gearNameUI;
        public float gearSteeringResponse;
        public float gearSteerResponse;
        public float rpmIncrement;
        public float rpmDecrement;
        public float minGearRPM;
        public float maxGearRPM;
        public float minGearVelocity;
        public float maxGearVelocity;
        public float gearRatio;
    }

    //Following are the Gear Structs that contain data for each gear
    GearData currentGear;
    GearData sGear_1;
    GearData sGear_2;
    GearData sGear_3;
    GearData sGear_4;
    GearData sGear_5;
    GearData sReverse;

    public enum TransmissionType { FWD, RWD, AWD };
    public TransmissionType CarTransmission = TransmissionType.FWD;

    //Following function accepts Acceleration Input
    public void Acceleration(InputAction.CallbackContext inputReceived)
    {
        //This if statement checks if the player pressed the Up arrow key
        if (inputReceived.started == true)
        {
            throttle = true;
            //If if the key was just pressed, Set the throttle to true
        }

        if (inputReceived.canceled == true)
        {
            throttle = false;
            //If the key was just released, Set the throttle to false
        }
    }

    //Following function accepts Steering Input
    public void Steering(InputAction.CallbackContext inputReceived)
    {
        steeringInput = inputReceived.ReadValue<float>();
    }

    //Following function accepts Brake Input
    public void Brakes(InputAction.CallbackContext inputReceived)
    {
        if (inputReceived.started == true)
        {
            brake = true;
        }

        if (inputReceived.canceled == true)
        {
            brake = false;
        }
    }

    //Following function accepts HandBrake Input
    public void HandBrake(InputAction.CallbackContext inputReceived)
    {
        if (inputReceived.started == true)
        {
            handBrake = true;
        }

        if (inputReceived.canceled == true)
        {
            handBrake = false;
        }
    }

    //Following function accepts Reverse Input
    public void Reverse(InputAction.CallbackContext inputReceived)
    {
        AttemptReverse();
    }

    //Following function accepts Nitrous Input
    public void Nitrous(InputAction.CallbackContext inputReceived)
    {
        if(inputReceived.started == true)
        {
            nitro = true;
        }
        if(inputReceived.canceled == true)
        {
            nitro = false;
        }
    }

    //Following function accepts Jump Input
    public void Jump(InputAction.CallbackContext inputReceived)
    {
        Jump();
    }

    //Following function resets the player to start position
    public void IA_ResetPlayer(InputAction.CallbackContext inputReceived)
    {
        ResetPlayer();
    }

    // Start is called before the first frame update
    //Start is being used to Initialise various Gear Related Values
    void Start()
    {
        //Setting the values of the First Gear Struct
        sGear_1.currentGearName = AvailableGears.eGear_1;
        sGear_1.gearSteerResponse = steerResponseG1;
        sGear_1.rpmIncrement = 200f;
        sGear_1.rpmDecrement = 20f;
        sGear_1.gearNameUI = "1";
        sGear_1.minGearRPM = idleRPM;
        sGear_1.maxGearRPM = 4500f;
        sGear_1.minGearVelocity = 0f;
        sGear_1.maxGearVelocity = 25f;
        sGear_1.gearRatio = apG1;

        //Setting the values of the Second Gear Struct
        sGear_2.currentGearName = AvailableGears.eGear_2;
        sGear_2.gearSteerResponse = steerResponseG2;
        sGear_2.rpmIncrement = 110f;
        sGear_2.rpmDecrement = 25f;
        sGear_2.gearNameUI = "2";
        sGear_2.minGearRPM = 1500f;
        sGear_2.maxGearRPM = 4900f;
        sGear_2.minGearVelocity = 25f;
        sGear_2.maxGearVelocity = 40f;
        sGear_2.gearRatio = apG2;

        //Setting the values of the Third Gear Struct
        sGear_3.currentGearName = AvailableGears.eGear_3;
        sGear_3.gearSteerResponse = steerResponseG3;
        sGear_3.rpmIncrement = 90f;
        sGear_3.rpmDecrement = 30f;
        sGear_3.gearNameUI = "3";
        sGear_3.minGearRPM = 1800f;
        sGear_3.maxGearRPM = 5300f;
        sGear_3.minGearVelocity = 40f;
        sGear_3.maxGearVelocity = 60f;
        sGear_3.gearRatio = apG3;

        //Setting the values of the Fourth Gear Struct
        sGear_4.currentGearName = AvailableGears.eGear_4;
        sGear_4.gearSteerResponse = steerResponseG4;
        sGear_4.rpmDecrement = 35f;
        sGear_4.rpmIncrement = 75f;
        sGear_4.gearNameUI = "4";
        sGear_4.minGearRPM = 2000f;
        sGear_4.maxGearRPM = 5800f;
        sGear_4.minGearVelocity = 60f;
        sGear_4.maxGearVelocity = 90f;
        sGear_4.gearRatio = apG4;

        //setting the values of the Fifth Gear Struct
        sGear_5.currentGearName = AvailableGears.eGear_5;
        sGear_5.gearSteerResponse = steerResponseG5;
        sGear_5.rpmIncrement = 70f;
        sGear_5.rpmDecrement = 40f;
        sGear_5.gearNameUI = "5";
        sGear_5.minGearRPM = 2500f;
        sGear_5.maxGearRPM = redRPM;
        sGear_5.minGearVelocity = 100f;
        sGear_5.maxGearVelocity = 150f;
        sGear_5.gearRatio = apG5;

        //Setting the values of the Reverse Gear
        sReverse.currentGearName = AvailableGears.eGear_reverse;
        sReverse.gearSteerResponse = steerResponseR;
        sReverse.rpmDecrement = 20f;
        sReverse.rpmIncrement = 65f;
        sReverse.gearNameUI = "R";
        sReverse.minGearRPM = 0f;
        sReverse.maxGearRPM = redRPM;
        sReverse.minGearVelocity = 0f;
        sReverse.maxGearVelocity = 45f;
        sReverse.gearRatio = apGR;

        //Setting the current Gear to Gear 1 at the start of the Game
        currentGear = sGear_1;
        currentNitroLeft = 100;
        currentNitroPower = 0;

        originalPos = CarBody.transform.position;
        originalRot = CarBody.transform.rotation;
    }

    // Update is called once per frame
    // Update is being used to call the UpdateRPM function and to copy the transform of wheel colliders to the meshes
    void Update()
    {
        UpdateRPM();
        UpdateUI();

        UpdateWheels(wc_FrontLeft, wm_FrontLeft);
        UpdateWheels(wc_FrontRight, wm_FrontRight);
        UpdateWheels(wc_RearLeft, wm_RearLeft);
        UpdateWheels(wc_RearRight, wm_RearRight);
    }

    // Fixed Update is called 50 times a frame
    // All of the physics related applications occur here
    private void FixedUpdate()
    {
        //Following function applies torque to the wheels
        ApplyTorque(CarTransmission);

        //Following function manages and applies nitro values
        UseNitrous();

        //This function applies the steering
        ApplySteering();

        //This funcrion applies Brakes
        ApplyBrakes();

        //This function applies HandBrakes
        ApplyHandbrakes();
    }

    // This function copies the transform of wheel colliders to the meshes
    void UpdateWheels(WheelCollider incomingCollider, Transform outgoingMesh)
    {
        Vector3 tempLocation;
        Quaternion tempRotation;

        incomingCollider.GetWorldPose(out tempLocation, out tempRotation);

        outgoingMesh.position = tempLocation;
        outgoingMesh.rotation = tempRotation;
    }

    // Calculates Engine RPM and calls functions to increment or decrement gears
    void UpdateRPM()
    {
        //Converts the car speed into Miles per hour and stores it in the variable called currentVelo
        currentVelo = CarBody.velocity.magnitude * 2.23f;

        //The following line increases RPM when the acceleration key is pressed. It also takes into account your horsepower
        if (throttle == true)
        {
            currentRPM = currentRPM + horsePower / currentGear.rpmIncrement;

            //Makes sure that the current RPM never exceeds the MAX rpm of a particular gear or the Red RPM if the current gear is the 5th gear
            if (currentRPM >= currentGear.maxGearRPM)
            {
                currentRPM = currentGear.maxGearRPM;
            }

            //If the currentRPM exceeds the redRPM of the engine, Go to the next gear if the velocity allows
            if (currentRPM >= currentGear.maxGearRPM && currentVelo >= currentGear.maxGearVelocity)
            {
                IncrementGear();
            }
        }

        if (throttle == false)
        {
            currentRPM -= currentGear.rpmDecrement;
        }

        if (currentRPM <= idleRPM)
        {
            currentRPM = idleRPM;
        }

        if (currentVelo <= currentGear.minGearVelocity)
        {
            DecrementGear();
        }
    }

    // Function update the UI Quantities
    void UpdateUI()
    {
        rpmBarUI.fillAmount = currentRPM / currentGear.maxGearRPM;
        nitrousBarUI.fillAmount = currentNitroLeft / 100f;
        speedUI.text = Mathf.FloorToInt(currentVelo).ToString();
        gearUI.text = currentGear.gearNameUI.ToString();

    }

    void ApplyTorque(TransmissionType l_transmission)
    {
        float appliedTorque = (((horsePower * 5252) / currentRPM) * currentGear.gearRatio) + ((-1)*currentNitroPower);
        //Debug.Log(appliedTorque);

        if (throttle)
        {
            switch (l_transmission)
            {
                case TransmissionType.FWD:
                    wc_FrontLeft.motorTorque = appliedTorque / 2;
                    wc_FrontRight.motorTorque = appliedTorque / 2;
                    break;

                case TransmissionType.RWD:
                    wc_RearLeft.motorTorque = appliedTorque / 2;
                    wc_RearRight.motorTorque = appliedTorque / 2;
                    break;
                case TransmissionType.AWD:
                    wc_FrontLeft.motorTorque = appliedTorque / 1.5f;
                    wc_FrontRight.motorTorque = appliedTorque / 1.5f;
                    wc_RearLeft.motorTorque = appliedTorque / 1.5f;
                    wc_RearRight.motorTorque = appliedTorque / 1.5f;
                    break;
            }

        }

        if (throttle == false)
        {
            wc_FrontLeft.motorTorque = 0;
            wc_FrontRight.motorTorque = 0;
            wc_RearLeft.motorTorque = 0;
            wc_RearRight.motorTorque = 0;
        }
    }

    void ApplySteering()
    {
        float steerAmount = 0;

        if(steeringInput < steerAmount)
        {
            steerAmount -= currentGear.gearSteerResponse;
        }
        else if (steeringInput > steerAmount)
        {
            steerAmount += currentGear.gearSteerResponse;
        }
        else steerAmount = 0;

        wc_FrontLeft.steerAngle = steerAmount * maxSteerAngle;
        wc_FrontRight.steerAngle = steerAmount * maxSteerAngle;
    }

    void ApplyBrakes()
    {
        if (brake == true)
        {
            wc_FrontLeft.brakeTorque = brakeStrength;
            wc_FrontRight.brakeTorque = brakeStrength;
        }

        if (brake == false)
        {
            wc_FrontLeft.brakeTorque = 0;
            wc_FrontRight.brakeTorque = 0;
        }
    }

    void ApplyHandbrakes()
    {
        if (handBrake == true)
        {
            wc_RearLeft.brakeTorque = handBrakeStrength;
            wc_RearRight.brakeTorque = handBrakeStrength;
        }

        if (handBrake == false)
        {
            wc_RearLeft.brakeTorque = 0;
            wc_RearRight.brakeTorque = 0;
        }
    }

    void IncrementGear()
    {
        switch (currentGear.currentGearName)
        {
            case AvailableGears.eGear_1:
                currentGear = sGear_2;
                currentRPM = currentGear.minGearRPM;
                break;

            case AvailableGears.eGear_2:
                currentGear = sGear_3;
                currentRPM = currentGear.minGearRPM;
                break;

            case AvailableGears.eGear_3:
                currentGear = sGear_4;
                currentRPM = currentGear.minGearRPM;
                break;

            case AvailableGears.eGear_4:
                currentGear = sGear_5;
                currentRPM = currentGear.minGearRPM;
                break;

            case AvailableGears.eGear_5:
                break;
        }
    }

    void DecrementGear()
    {
        switch (currentGear.currentGearName)
        {
            case AvailableGears.eGear_5:
                currentGear = sGear_4;
                break;

            case AvailableGears.eGear_4:
                currentGear = sGear_3;
                break;

            case AvailableGears.eGear_3:
                currentGear = sGear_2;
                break;

            case AvailableGears.eGear_2:
                currentGear = sGear_1;
                break;

            case AvailableGears.eGear_1:
                break;
        }
    }

    void AttemptReverse()
    {
        if (reverse == false && currentVelo <= 5)
        {
            reverse = true;
            currentGear = sReverse;
            return;
        }

        else if (reverse == true)
        {
            reverse = false;
            currentGear = sGear_1;
            return;
        }
    }

    void UseNitrous()
    {
        float nitrodecrement = 100 / (nitroDuration * 50);
        float nitroIncrement = nitrodecrement / 2;

        //If button is pressed, and nitro is not empty, Set nitro value to what it needs to be and reduce the nitro that's left
        if (nitro == true && currentNitroLeft > 0)
        {
            currentNitroPower = nitroPower;
            nitroOffTime = 0;

            currentNitroLeft = currentNitroLeft - nitrodecrement;
        }
        
        //If button is not pressed, set the added amount back to 0 and start counting off time
        if(nitro == false)
        {
            currentNitroPower = 0;
            nitroOffTime = nitroOffTime + 1;
        }

        //If button has not been pressed for a certain amount of time, start recharging nitrous
        if(nitro == false && nitroOffTime >= 100)
        {
            if(currentNitroLeft<= 100)
            {
                currentNitroLeft = currentNitroLeft + nitroIncrement;
            }

        }
    }

    void Jump()
    {
        bool isOnGround = (wc_FrontLeft.isGrounded && wc_FrontRight.isGrounded && wc_RearLeft.isGrounded && wc_RearRight.isGrounded);
        
        if (isOnGround == true)
        {
            CarBody.AddForce(transform.up * 1000000);
        }
    }

    void ResetPlayer()
    {
        CarBody.transform.position = originalPos;
        CarBody.transform.rotation = originalRot;
    }
}
