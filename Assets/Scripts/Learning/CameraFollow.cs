using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject FollowTarget;
    public float cameraHeight;
    public float cameraDistance;
    public float smoothTime = 0.3f;
    private Vector3 velo = Vector3.zero;
    private Vector3 desiredLocation;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        desiredLocation = FollowTarget.transform.TransformPoint(0, cameraHeight, cameraDistance);

        transform.position = Vector3.SmoothDamp(transform.position, desiredLocation, ref velo, smoothTime);
        transform.LookAt(FollowTarget.transform.position);
    }
}
